import React from "react";
import { Redirect } from 'react-router-dom'
import axios from "axios";

class Signin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            items: [],
        }
        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        var token = null
        if (localStorage.getItem('DATAUSER') == null) {
            this.setState({
                redirect: true
            })
        } else {
            token = JSON.parse(localStorage.getItem('DATAUSER')).token
            axios.get(`${window.baseURL}/rickandmortyapi`, {
                headers: {
                    'Authorization': token
                }
            })
                .then(result => {
                    if (result.data.error) {
                        console.log('ERROR', result)
                    } else {
                        this.setState({
                            items: result.data.data
                        })
                    }
                })
                .catch(err => {
                    console.log("ERROR:", err.message)
                    localStorage.removeItem('DATAUSER')
                    this.setState({
                        redirect: true
                    })
                })
        }
    }

    renderRedirect() {
        if (this.state.redirect) {
            return (
                <Redirect to="/"></Redirect>
            )
        }
    }

    logout() {
        localStorage.removeItem('DATAUSER');
        this.setState({
            redirect: true
        });
    }

    render() {
        return (
            <div className="row">
                {this.renderRedirect()}
                <div className="col-10 mx-auto">
                    <div className="row">
                        <div className="card-body d-flex justify-content-between align-items-center">

                            <div>
                                BIENVENIDO(A) &nbsp;
                                <b>
                                    {
                                        JSON.parse(localStorage.getItem('DATAUSER')) == null ?
                                        "" : JSON.parse(localStorage.getItem('DATAUSER')).username.toUpperCase()
                                    }
                                </b>
                            </div>

                            <button className="btn btn-primary mb-2" onClick={this.logout}>
                                CERRAR SESION
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col-10 mx-auto">
                    <div className="card-columns">
                        {this.state.items.map((i) => {
                            return (
                                <div key={i.id} className="card" style={cardWidth}>
                                    <img className="card-img-top" src={i.image}></img>
                                    <div className="card-body">
                                        <h4 className="card-title">Name: {i.name}</h4>
                                        <p className="card-text">Status: {i.status}</p>
                                        <p className="card-text">Species: {i.species} </p>
                                        <p className="card-text">Gender: {i.gender}</p>
                                    </div>
                                </div>                                
                            )
                        })}
                    </div>
                </div>
            </div>
        );
    };
};

const cardWidth = {
    width:' 340px'
}

export default Signin;