import React from "react";
import { Redirect } from 'react-router-dom'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';

class Signin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            message: '',
            success: null,
            redirect: false,
            username: null,
            password: null
        }
        this.register = this.register.bind(this);
    }

    componentDidMount() {
        if (localStorage.getItem('DATAUSER') != null) {
            this.setState({
                redirect: true
            })
        }
    }

    handleusername(e) {
        this.setState({
            username: e.target.value
        });
    }

    handlePassword(e) {
        this.setState({
            password: e.target.value
        })
    }

    register() {

        if(!this.state.username || !this.state.password){
            this.setState({
                error: true,
                success: false,
                message: 'Complete todo los campos.'
            })
        } else {
            axios.post(`${window.baseURL}/register`, {
                username: this.state.username,
                password: this.state.password
            })
            .then(result => {
                if(result.data.error) {
                    this.setState({
                        error: true,
                        success: false,
                        message: `"${this.state.username}" ya esta en uso.`
                    })
                } else {
                    this.setState({
                        success: true,
                        error: false
                    })
                }
            })
            .catch(err =>{
                console.log("ERROR",err)
            })
        }
    }

    renderRedirect() {
        if(this.state.redirect) {
            return (
                <Redirect  to="/home"></Redirect>
            )
        }
    }

    renderError() {
        if(this.state.error) {
            return (
                <div className="alert alert-danger" role="alert">
                    <small>{this.state.message}</small>
                </div>
            )
        }
    }

    renderSuccess() {
        if(this.state.success) {
            return (
                <div className="alert alert-success" role="alert">
                    <small>"Registro exitoso!.</small>
                </div>
            )
        }
    }

    render() {
        return (
            <div className="container my-auto">
                {this.renderRedirect()}
                <div className="row">
                    <div id="login" className="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12">
                        <h2 className="text-center">Registro de Usuario</h2>
                        <img className="img-fluid mx-auto d-block rounded" src="http://mouse.latercera.com/wp-content/uploads/2019/05/rick-morty.jpg"></img>
                        <div className="form-group">
                            <label>Usuario</label>
                            <input
                                id="username"
                                name="username"
                                className="form-control"
                                type="email"
                                placeholder="Usuario"
                                onChange={(e) => this.handleusername(e)}
                                required
                            ></input>
                        </div>
                        <div className="form-group">
                            <label>Contraseña</label>
                            <input
                                id="password"
                                name="password"
                                className="form-control"
                                type="password"
                                placeholder="Contraseña"
                                onChange={(e) => this.handlePassword(e)}
                                required
                            ></input>
                        </div>

                        <div className="text-right">
                            <Link to="/">Iniciar sesion</Link>
                        </div>

                        <button className="btn btn-primary btn-block mb-2" onClick={this.register}>
                            REGISTRARME
                        </button>

                        {this.renderError()}
                        {this.renderSuccess()}
                    </div>
                </div>
            </div>
        );
    };
};

export default Signin;