import React from "react";
import { Redirect } from 'react-router-dom'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'
import { hot } from "react-hot-loader";
import "./App.css";

import Home from './pages/Home';
import Signin from './pages/Signin';
import Signup from './pages/Signup';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    }
    // window.baseURL = 'http://172.16.31.114:8081'
    window.baseURL = 'http://localhost:8081'
  }

  componentDidMount() {
    console.log('componentDidMount')
    if (localStorage.getItem('DATAUSER') != null) {
      this.setState({
        redirect: true
      })
    } else {
      localStorage.removeItem('DATAUSER')
    }
  }

  renderRedirect() {
    if (this.state.redirect) {
      return (
        <Redirect to="/home"></Redirect>
      )
    }
  }

  render() {
    return (
      <Router>
        {this.renderRedirect()}
        <Route path='/' exact component={Signin}/>
        <Route path='/home' component={Home} />
        <Route path='/signup' component={Signup} />
      </Router>
    );
  }
}

export default hot(module)(App);